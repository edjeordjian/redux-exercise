import { useSelector } from 'react-redux';
import { setNumber, saveToStack,
  sum, substraction, multiplication, division,
  undo, sumAll, squareRoot } from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';
import { store } from "../state/store";
import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const onClickNumber = (number) => {
    store.dispatch( setNumber(number) );
  };

  const onClickDot = () => {
    store.dispatch( setNumber(".") );
  };

  const onClickSum = () => {
    store.dispatch( sum() );
  };

  const onClickSubstraction = () => {
    store.dispatch( substraction() );
  };

  const onClickMultiplication = () => {
    store.dispatch( multiplication() );
  };

  const onClickDivision = () => {
    store.dispatch( division() );
  };

  const onClickSumAll = () => {
    store.dispatch( sumAll() );
  };

  const onClickSqrt = () => {
    store.dispatch( squareRoot() );
  };

  const onClickUndo = () => {
    store.dispatch( undo() );
  };

  const onClickIntro = () => {
    store.dispatch( saveToStack() );
  };

  return (
    <div className={styles.main}>
      {/*Textbox*/}
      <div className={styles.display}>
        {currentNumber}
      </div>

      {/*Number and dot keys*/}
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber( (i + 1).toString() )}>
            {i + 1}
          </button>
        ))}

        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>

        <button onClick={() => onClickDot()}>
            .
        </button>
      </div>

      {/*Operation keys*/}
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickSum()}>+</button>
        <button onClick={() => onClickSubstraction()}>-</button>
        <button onClick={() => onClickMultiplication()}>x</button>
        <button onClick={() => onClickDivision()}>/</button>
        <button onClick={() => onClickSqrt()}>√</button>
        <button onClick={() => onClickSumAll()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>

      {/*Number stack*/}
      <div className={styles.stack}>
        { stack.map(renderStackItem) }
      </div>
    </div>
  );
};
