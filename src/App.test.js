import App from './App';
import {render, fireEvent, screen} from '@testing-library/react'

test('Display', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  expect( screen.getByText("10") ).toBeInTheDocument();
});

test('Sum', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("+") );
  expect( screen.getByText("60") ).toBeInTheDocument();
});

test('Substraction', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("-") );
  expect( screen.getByText("-40") ).toBeInTheDocument();
});

test('Multiplication', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("x") );
  expect( screen.getByText("500") ).toBeInTheDocument();
});

test('Division', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("/") );
  expect( screen.getByText("0.2") ).toBeInTheDocument();
});

test('Sum all', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("Σ") );
  expect( screen.getByText("126") ).toBeInTheDocument();
});

test('sqrt', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("7") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("√") );
  expect( screen.getByText("2.6457513110645907") ).toBeInTheDocument();
});

test('history', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("/") );
  fireEvent.click( screen.queryByText("Undo") );
  expect( screen.getByText("50") ).toBeInTheDocument();
});

test('Avoid double dot', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText(".") );
  fireEvent.click( screen.queryByText(".") );
  fireEvent.click( screen.queryByText("0") );
  expect( screen.getByText("1.0") ).toBeInTheDocument();
});

test('Operation with element in the buffer', () => {
  render(<App/>);
  fireEvent.click( screen.queryByText("1") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("Intro") );
  fireEvent.click( screen.queryByText("5") );
  fireEvent.click( screen.queryByText("0") );
  fireEvent.click( screen.queryByText("+") );
  expect( screen.getByText("60") ).toBeInTheDocument();
});