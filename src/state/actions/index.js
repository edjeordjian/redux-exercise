export const setNumber = (number) => ({
    type: 'SET_NUMBER',
    expression: number,
});

export const saveToStack = () => ({
    type: 'SAVE_NUMBER',
});

export const sum = () => ({
    type: 'SUM',
});

export const substraction = () => ({
    type: 'SUBSTRACTION',
});

export const multiplication = () => ({
    type: 'MULTIPLICATION',
});

export const division = () => ({
    type: 'DIVISION',
});

export const sumAll = () => ({
    type: 'SUM_ALL',
});

export const squareRoot = () => ({
    type: 'UNARY_SQUARE_ROOT',
});

export const undo = () => ({
    type: 'UNDO',
});
