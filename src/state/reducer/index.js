const doOperation = (state, operation) => {
  if (state.now.stack.length === 0) {
    state.history.pop();
    return;
  }

  if (state.now.expression.length !== 0) {
    state.now.stack.push(state.now.expression);
  }

  if ( state.now.stack.length < 2 && ! operation.startsWith("UNARY_") ) {
    state.history.pop();
    return;
  }

  let result = 0;

  if (operation === "SUM") {
    result = Number( state.now.stack.pop() ) +
      Number( state.now.stack.pop() );
  }

  else if (operation === "SUBSTRACTION") {
    result = - Number( state.now.stack.pop() ) +
      Number( state.now.stack.pop() );
  }

  else if (operation === "MULTIPLICATION") {
    result = Number( state.now.stack.pop() ) *
      Number( state.now.stack.pop() );
  }

  else if (operation === "DIVISION") {
    let dividend = Number( state.now.stack.pop() );
    result = Number( state.now.stack.pop() ) / dividend;
  }

  else if (operation === "SUM_ALL") {
    state.now.stack.forEach( (x) => {
      result += Number(x);
    } );
    state.now.stack = [];
  }

  else if (operation === "UNARY_SQUARE_ROOT") {
    result = Math.sqrt( Number( state.now.stack.pop() ) );
  }

  state.now.stack.push( result.toString() );
  // Just to update the visual component.
  state.now.expression = new String(" ");
}

export const rootReducer = (state, action) => {
  if (state === undefined) {
    state = {
      now: { 
        expression: "",
        stack: [],
      },
      history: []
    };
  }

  else if( state.now.expression.startsWith(" ") ) {
    state.now.expression = "";
  }

  switch (action.type) {
    case 'SET_NUMBER':
      if (action.expression === "." && state.now.expression.includes(".")) {
        state.history.pop();
        break;
      }

      if (state.now.expression.length === 18) {
        state.history.pop();
        break;
      }

      state.now.expression += action.expression
      break;

    case 'SAVE_NUMBER':
      if (state.now.stack.length === 17 || state.now.expression === "") {
        state.history.pop();
        break;
      }

      state.now.stack.push(state.now.expression);
      state.now.expression = "";
      break;

    case 'SUM':
    case 'SUBSTRACTION':
    case 'MULTIPLICATION':
    case 'DIVISION':
    case 'SUM_ALL':
    case "UNARY_SQUARE_ROOT":
      doOperation(state, action.type);
      break;

    case 'UNDO':
      if (state.history.length === 1) {
        state.history.pop();
        break;
      }

      state.history.pop();
      state.now = state.history.pop();
      break;

    default:
      break;
  }

  state.history.push( JSON.parse( JSON.stringify(state.now) ) );
  return state;
};
