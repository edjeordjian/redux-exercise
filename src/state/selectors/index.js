export const selectCurrentNumber = (state) => {
  if (state.now.expression === undefined) {
    return "";
  }

  return state.now.expression;
};

export const selectCurrentStack = (state) => {
  if (state.now.stack === undefined) {
    return [];
  }

  return state.now.stack;
};
